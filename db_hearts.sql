-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Хост: 127.0.0.1
-- Час створення: Гру 24 2013 р., 09:10
-- Версія сервера: 5.5.27
-- Версія PHP: 5.4.7

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- БД: `db_hearts`
--

-- --------------------------------------------------------

--
-- Структура таблиці `comments`
--

CREATE TABLE IF NOT EXISTS `comments` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `date_post` datetime NOT NULL,
  `heart_id` int(10) unsigned NOT NULL,
  `author` varchar(50) NOT NULL,
  `text` text NOT NULL,
  `rating` int(10) NOT NULL,
  `visible` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=43 ;

--
-- Дамп даних таблиці `comments`
--

INSERT INTO `comments` (`id`, `date_post`, `heart_id`, `author`, `text`, `rating`, `visible`) VALUES
(39, '2013-12-06 16:25:41', 42, 'Test Test', 'hjk', 0, 1),
(40, '2013-12-06 16:26:02', 42, 'Test Test', 'asdfasdf', 0, 1),
(41, '2013-12-06 16:26:09', 42, 'Test Test', 'sdfasdf', 0, 1),
(42, '2013-12-06 16:26:09', 26, 'Test Test', 'sdfasdf', 0, 1);

-- --------------------------------------------------------

--
-- Структура таблиці `hearts`
--

CREATE TABLE IF NOT EXISTS `hearts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created` datetime NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `visible` tinyint(1) NOT NULL,
  `title` varchar(255) NOT NULL,
  `params` text NOT NULL,
  `img` text NOT NULL,
  `votes` int(10) NOT NULL,
  `text` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=43 ;

--
-- Дамп даних таблиці `hearts`
--

INSERT INTO `hearts` (`id`, `created`, `user_id`, `visible`, `title`, `params`, `img`, `votes`, `text`) VALUES
(26, '2013-10-28 11:29:18', 1, 1, 'Анатолий Поденщиков', '{"left":"280.9088134765625","top":"263.9088134765625","width":"50","height":"50","rotate":"150","color":"#ff0000"}', 'http://www.wallpaperswala.com/wp-content/gallery/cat/cat_wallpapers_46.jpg', -2, '<p>Ну что сказать об этом человеке? Отзывчив, добр и скромен. И кот.</p>'),
(27, '2013-10-28 11:32:14', 1, 0, 'Анатолий Поденщиков', '{"left":"37.195220947265625","top":"13.195220947265625","width":"100","height":"100","rotate":"130","color":"#00ff5a"}', 'http://www.wallpaperswala.com/wp-content/gallery/cat/cat_wallpapers_46.jpg', 3, '<p>Ну что сказать об этом человеке? Отзывчив, добр и скромен. И кот.</p>'),
(42, '2013-12-06 16:17:43', 2, 1, 'test', '{"left":"144.7078857421875","top":"266.7078552246094","width":"50","height":"50","rotate":"0","color":"#ff887c"}', 'http://hearts.dev/upload/1/HTML5.png', 11, 'afadfasdfadf');

-- --------------------------------------------------------

--
-- Структура таблиці `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `registered` date NOT NULL,
  `login` varchar(50) NOT NULL,
  `password` varchar(255) NOT NULL,
  `salt` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `hearts` int(10) NOT NULL,
  `likes` int(10) NOT NULL,
  `visits` int(10) NOT NULL,
  `rating` float NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Дамп даних таблиці `users`
--

INSERT INTO `users` (`id`, `registered`, `login`, `password`, `salt`, `email`, `name`, `hearts`, `likes`, `visits`, `rating`) VALUES
(1, '2013-10-29', 'vmatveev', '96e79218965eb72c92a549dd5a330112', '', 'mat-vay@meta.ua', 'Анатолий Поденщиков', 7, 26, 30, 63),
(2, '2013-10-29', 'test', '96e79218965eb72c92a549dd5a330112', '', 'test@gmail.com', 'Test Test', 11, 27, 10, 48);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
