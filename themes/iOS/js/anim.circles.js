var m = {};
m.type = ["rt", "rb", "lt", "lb"];
m.colors = ["254, 0, 0", "0, 255, 91", "255, 250, 4", "255, 158, 4"];
m.getRandType = function(){
	return m.type[rand(0,m.type.length - 1)];
};
m.getRandColor = function(){
	return m.colors[rand(0, m.colors.length - 1)];
};
function rand(min ,max){
	return Math.floor(Math.random() * (max - min + 1)) + min;
}

function createMObj(){
	$mChild = $(".mChild");
	if($mChild.length < 30){
		if($(".MObj").length == 0){
			$("#screen").prepend("<div class='MObj'></div>");
		}

		var mSize = rand(25, 100);
		var nTop = rand(0, $(window).height() - mSize);
		var nLeft = rand(0, $(window).width() - mSize);
		var rColor = m.getRandColor();
		var rOpacity = rand(8,13)/100;
		var vector = m.getRandType();
		$(".MObj").append("<div class='mChild'></div>");
		var el = $(".mChild").eq($(".mChild").length - 1);
		el.attr("vector", vector);
		el.css({ boxShadow: "0 0 30px rgba("+rColor+", "+(rOpacity + 0.1)+")", position: "absolute", width: mSize, height: mSize, borderRadius: 100, background: "rgba("+rColor+", "+rOpacity+")", top: nTop, left: nLeft});
	}
}
function removeMObj(){
	$(".mChild").random().remove();
}
function moveMObj(){
	$(".mChild").each(function(){
		var el = $(this);
		if( el.offset().left == 0 || el.offset().top == 0 || (el.offset().left + el.width()) >= $(window).width() || (el.offset().top + el.height()) >= $(window).height() ){
			el.animate({opacity: 0}, 500, function(){
				el.remove();
			})
		}

		if(el.attr("vector") == "rb"){
			el.css({ top:  el.offset().top + 1, left: el.offset().left + 1});
		}else if(el.attr("vector") == "rt"){
			el.css({ top:  el.offset().top - 1, left: el.offset().left + 1});
		}else if(el.attr("vector") == "lt"){
			el.css({ top:  el.offset().top + 1, left: el.offset().left - 1});
		}else if(el.attr("vector") == "lb"){
			el.css({ top:  el.offset().top - 1, left: el.offset().left - 1});
		}
	})

}
$( document ).ready(function() {
	setInterval("moveMObj();", 100);
	setInterval("createMObj();", 1000);
});