window.hideanim = null;

jQuery.fn.random = function() {
	var randomIndex = Math.floor(Math.random() * this.length);
	return jQuery(this[randomIndex]);
};

hearts = [];
intersection = 0;
menu_lock = 0;

var h = {};
h.id = "new";
h.title = "Вася";
h.img = "http://www.wallpaperswala.com/wp-content/gallery/cat/cat_wallpapers_46.jpg";
h.text = "<p>Ну что сказать об этом человеке? Отзывчив, добр и скромен. И кот.</p>";
h.votes = 0;

h.params = {};
h.params.left = 0;
h.params.top = 0;
h.params.width = 50;
h.params.height = 50;
h.params.rotate = 0;
h.params.color = "#ff0000";

$( document ).ready(function() {

	width_document = document.body.clientWidth;

	$("#wantheart").click(function(){
		hideMainLeftMenu();
		$("#heart_editor").css({ opacity: 0, left: -350, position: "relative"}).show().animate({ left:0, opacity: 1 })
	});

	$("#heart_editor .remove-btn").click(function(){
		$("#heart_editor").css({ left:0, opacity: 1 }).show().animate({  opacity: 0, left: -350})
		showMainLeftMenu();
	})

	info = $("#info");
	ipadScreen = $("#screen");
	body = $("body");
	comments = $("#comments_list");
	$header = $('#header');

	getAllHearts();
	drawDemoHeart();

	ipadScreen.on('tap', function() {
		$header.show(1000);
		if (!menu_lock)
			setTimeout(function() {
				$header.hide(1000);
				menu_lock = 0;
			}, 5000);
		menu_lock = 1;
	});

	$("#activity").css({ opacity: 0, position: "relative", left: -350 }).animate( {opacity: 1, left: 0}, 1000, 'swing' );
    $("#tophearts").css({ opacity: 0, position: "relative", left: -350 }).animate( {opacity: 1, left: 0}, 1000, 'swing' );
	//$("#heart_editor").css({ opacity: 0, position: "relative", right: 100 }).animate( {opacity: 1, right: 0}, 1000, 'swing' );
	$("#login-btn").on('click', initLoginForm);

	$("#heart_color").on('change', function() {
		h.params.color = $(this).val();
		drawDemoHeart();
	});
	$("#heart_size").on('change', function() {
		h.params.width = h.params.height = $(this).val();
		drawDemoHeart();
	});
	$("#heart_turn_r").on("click", function(){
		h.params.rotate += 10;
		drawDemoHeart();
	});
	$("#heart_turn_l").on("click", function(){
		h.params.rotate -= 10;
		drawDemoHeart();
	});
});

function hideMainLeftMenu(){
	$("#right").css({
		opacity: 1,
		top: 0
	}).animate( {
		opacity: 0,
		top: 500
	}, 500,function(){
		$(this).css({display: 'none'});
	} );
}
function showMainLeftMenu(){
	$("#right").css({
		opacity: 0,
		top: 500,
		display: 'block'
	}).animate( {
		opacity: 1,
		top: 0
	}, 500);
}

function getAllHearts() {
	ipadScreen.empty();
	$.ajax({
		type: "POST",
		url: "" +
			"hearts/getall",
		dataType: 'json'
	}).done(function( res ) {
		if (res['e'] == 0) {
		hearts = res['r'];
		for (var i=0; i<hearts.length; i++) {
		drawHeart(hearts[i]);
		}
		setInterval(function () {
			animateHeart($(".h").random());
		} , 70);
	}
	else {
		notification("error", "server error");
		//console.log(res['e']);
	}
	});
}
function drawDemoHeart() {
	var heart = createHeart(h);
	var screen = $("#screen_editor");
	heart.addClass("demo");
	screen.empty().append(heart);
}
function saveHeart() {
	if (intersection) {
		notification("error", "crossing should not be");
	}
	else {
		$("#center").removeClass("editable");
		$(".h").removeClass("obstacle");

		h.params.left = newHeart.offset().left - newHeart.parent().offset().left;
		h.params.top = newHeart.offset().top - newHeart.parent().offset().top;

		$.ajax({
			type: "POST",
			url: "hearts/save",
			dataType: 'json',
			data: {heart: h}
		}).done(function( res ) {
			if (res['e'] == 0) {
				notification("success", "heart created");
				//console.log(res['r']);
				getAllHearts();
				//newHeart.id = "h"+res['r']['id'];
			}
			else {
				notification("error", res['d']);
				//console.log(res['d']);
			}
		});
	}
}

function updateHeart() {
	$.ajax({
		type: "POST",
		url: "hearts/update",
		dataType: 'json',
		data: {heart: h}
	}).done(function( res ) {
		if (res['e'] == 0) {
		notification("success", "heart updated");
		//console.log(res['r']);
		}
		else {
			notification("success", res['d']);
			//console.log(res['d']);
		}
	});
}

function newHeart() {

	$("#center").addClass("editable");
	$(".h").addClass("obstacle");
	newHeart = drawHeart(h);
	newHeart.draggable({ addClasses: false, containment: "parent"});
	newHeart.each(function(){$(this).bind("drag", function(){
			$(".obstacle").removeClass("cross");
			newHeart.removeClass("cross");
			intersection = 0;
			var hearts = newHeart.collision( ".obstacle", { relative: "collider", obstacleData: "odata", colliderData: "cdata", directionData: "ddata", as: "<div/>" });
			for( var i=0; i<hearts.length; i++ ) {
				var cross_id = $($(hearts[i]).data("odata")).get(0).id;
				$('#'+cross_id).addClass("cross");
				intersection++;
			}
			if (hearts.length) newHeart.addClass("cross");
		});
	});
}
function createHeart(arg) {
	var svg = '<svg viewBox="0 0 170 170" xmlns="http://www.w3.org/2000/svg">'+
		'<g><g id="svg_11">'+
		'<g transform="matrix(0.248062 0 0 0.248062 0 0)" id="svg_7">'+
		'<path fill="'+arg.params.color+'" id="svg_9" d="m329.547,606.48499c-13.77499,-15.43994 -48.16998,-45.53003 -76.43399,-66.88c-83.744,-63.23999 -95.142,-72.39001 -129.1438,-103.69995c-62.68451,-57.72003 -89.30559,-115.71106 -89.21436,-194.341c0.04451,-38.383 2.66077,-53.17102 13.40985,-75.797c18.2367,-38.38605 45.1003,-66.90906 79.4453,-84.354c24.325,-12.35602 36.323,-17.84601 76.944,-18.07001c42.494,-0.23505 51.43901,4.71997 76.43501,18.45197c30.42499,16.71399 61.73999,52.435 68.21298,77.81l3.99899,15.67303l9.85901,-21.58502c55.716,-121.97302 233.599,-120.14801 295.50201,3.03198c19.638,39.07605 21.79401,122.513 4.38098,169.513c-22.716,61.30701 -65.38,108.04706 -164.00699,179.67706c-64.681,46.96997 -137.88501,118.04993 -142.98001,128.02991c-5.91599,11.58008 -0.28299,1.81006 -26.409,-27.45996z" />'+
		'<g id="svg_8"/>'+
		'</g>'+
		'</g></g>'+
		'</svg>';
	var heart =  $("<div id='h"+arg.id+"'></div>");

	heart.data("data", arg);
	heart.css( "left", arg.params.left + "px" );
	heart.css( "top", arg.params.top + "px");
	heart.css( "width", arg.params.width  + "px");
	heart.css( "height", arg.params.height  + "px");
	heart.css({transform : "rotate("+arg.params.rotate+"deg)"});
	heart.data("params", arg.params);
	heart.append(svg);

	return heart;
}
function drawHeart(arg) {

	var heart = createHeart(arg);

	heart.addClass("h");
	heart.each(function() {
	var z = $(this);//.children("svg")
	setTimeout(function() {
		z.css({opacity: 0}).animate({opacity: 1}, 0.5+Math.random()*2000, 'swing',
		function() {});
	},
	Math.random()*1000);
	});
	heart.bind("tap", function() {
		getComments(arg.id);
		$("#send-mess").on("click", function(){
			sendComment(arg.id);
		});
		var h = $(this);
		info.finish();
		has = h.hasClass("hselected");
		$(".h").removeClass("hselected");
		if (!has) {
			h.toggleClass("hselected");
			heartHover(this);
			info.animate({opacity: 1}, 300, "swing", function() {  });
		}
		else {
			info.animate({opacity: 0.7}, 300, "swing", function() {  });
			heartUnHover(this);
		}
	});

	ipadScreen.append(heart);

	return heart;
}
function heartHover(this_heart){
	var arg = $(this_heart).data("data");
	var html = "<h3>"+arg.title+"</h3>"+
		"<img src='"+arg.img+"' />"+
		"<div>"+arg.text+"</div>"+
		"<button class='remove-btn' onclick='removeSelected()'><i class='fa fa-times'></i></button>"+
		"<div id='infobottom'>"+
		"<button class='info-btn left' onclick='vote(1, "+arg.id+")'><i class='fa fa-thumbs-o-up'></i></button>"+
		"<span class='likes'>"+arg.votes+"</span>"+
		"<button class='info-btn right' onclick='vote(-1, "+arg.id+")'><i class='fa fa-thumbs-o-down'></i></button>"+
		"</div>";
	info.html(html);
	var h = $(this);
	info.finish();
	var left = arg.params.left;
	console.log(left);
	left = (left > width_document/2) ? 0 : (width_document-info.outerWidth());
	info.css("display", "block");
	info.css({left: left});
	//if ($(".hselected").size() == 0)
	info.animate({opacity: 0.7}, 300, "swing", function() {  });
	$("#h" + arg.id).addClass("hhover");
}
function heartUnHover(this_heart){
	var arg = $(this_heart).data("data");
	$("#h" + arg.id).removeClass("hhover");
	if ($(".hselected").size() == 0)
		info.animate({opacity: 0}, 100, "swing", function() { info.css("display", "none"); });
}
function animateHeart(x) {
	var params = x.data("params");
	var ang = parseInt(params.rotate) + (Math.random()*2-1)*0.5;
	var scale = 1 + (Math.random()*2-1)*0.03;
	//console.log(ang);
	x.css({transform : "rotate("+ang+"deg) scale("+scale+")"});
}

function vote(arg, id){
	var likes  = $(".likes");
	var val = parseInt(likes.text());
	if (arg == 1)
		likes.text(val+1);
	else
		likes.text(val-1);
	$.ajax({
		type: "POST",
		url: "hearts/vote",
		dataType: "json",
		data: {id: id, vote: arg}
	}).done(function(res){
		//console.log(res);
	});
}

function removeSelected() {
	$(".h").removeClass("hselected");
	info.animate({opacity: 0}, 300, "swing", function() {
		info.css("display", "none");
	});
}

function notification(type, msg) {
	var note = $("<div class='notification'></div>");
	note.html(msg);
	switch(type) {
		case "info":
			note.css({borderColor: "#1016FF", backgroundColor: "#A8D9FF"});
			break;
		case "warning":
			note.css({borderColor: "#FFFF00", backgroundColor: "#FFF4AF"});
			break;
		case "error":
			note.css({borderColor: "#FF0000", backgroundColor: "#FFAD99"});
			break;
		case "success":
			note.css({borderColor: "#008000", backgroundColor: "#C2FF9D"});
			break;
	}
	note.css({ opacity: 0}).animate( {opacity: 1}, 2000, 'swing' );
	body.append(note);
	setTimeout(function(){
		note.css({ opacity: 1}).animate( {opacity: 0}, 2000, 'swing', function(){
			note.remove();
		});
	}, 2000);
}
function initLoginForm() {
	hideMainLeftMenu();
	$("#sign-up-form").css({display: "none"});
	$("#login-form").css({display: "block"});
	$("#form").css({display: "block", opacity: 0, left: -100}).animate({opacity: 1, left: 30}, 500, 'swing', function(){
		$("#form_login").focus();
		$("#remove-login").on('click', function(){
			showMainLeftMenu();
			$("#form").animate( {opacity: 0, right: -292}, 500, 'swing', function(){
				//$(this).css({display: "none"});
				//$("#login-form").css({display: "none"});
			});
		});
		$('#form_pass').bind('keypress', function(e) {
			var code = e.keyCode || e.which;
			if(code == 13) {
				login();
			}
		});
	});
}
function initSignUpForm() {
	$("#login-form").css({display: "none"});
	$("#sign-up-form").css({display: "block"});
	$("#form").css({display: "block", opacity: 0, right: -292}).animate({opacity: 1, right: 0}, 500, 'swing', function(){
		$("#sign_up_email").focus();
		$("#remove-sign-up").on('click', function(){
			showMainLeftMenu();
			$("#form").animate( {opacity: 0, right: -292}, 500, 'swing', function(){
				//$(this).css({display: "none"});
				//$("#sign-up-form").css({display: "none"});
			});
		});
		$('#sign_up_rpass').bind('keypress', function(e) {
			var code = e.keyCode || e.which;
			if(code == 13) {
				signUp();
			}
		});
	});
}
 function login(){
	 var data = {};
	 data['login'] = $("#form_login").val();
	 data['password'] = $("#form_pass").val();
	 $.ajax({
		 type: "POST",
		 url: "user/login",
		 dataType: "JSON",
		 cache: false,
		 data: {LoginForm: data}
	 }).done(function( res ){
		 if (res['e']) {
			 var html = "";
			 for (var key in res['d']) {
				 html += "<p>"+res['d'][key]+"</p>";
			 }
			 notification("error", html);
			 $("#login-form").addClass("error-form");
			 setTimeout(function(){
				 $("#login-form").removeClass("error-form");
			 }, 1000);
		 }
		 else {
			 location.reload();
		 }
	 });
 }
function signUp() {
	var data = {};
	data['email'] = $("#sign_up_email").val();
	data['login'] = $("#sign_up_login").val();
	data['name'] = $("#sign_up_name").val();
	data['password'] = $("#sign_up_pass").val();
	$.ajax({
		type: "POST",
		url: "user/signup",
		dataType: "JSON",
		cache: false,
		data: {User: data}
	}).done(function( res ){
		if (res['e']) {
			var html = "";
			for (var key in res['d']) {
				html += "<p>"+res['d'][key]+"</p>";
			}
			notification("error", html);
			$("#sign-up-form").addClass("error-form");
			setTimeout(function(){
				$("#sign-up-form").removeClass("error-form");
			}, 1000);
		}
	});
}

function getComments(id) {
	$.ajax({
		type: "POST",
		url: "comments/get",
		dataType: "JSON",
		cache: false,
		data: {id: id}
	}).done(function(res){
		comments.empty();
		if (res['e']) {
			console.log(res['d']);
		}
		else {
			for (var i=0; i<res['r'].length; i++) {
				drawComment(res['r'][i]);
			}
		}
	});
}
function drawComment(data){
	var html;
	html = '<li class="comment">' +
				'<div class="head_comment">' +
					'<span class="user_name">'+data['author']+'</span>'+
					'<span class="date">'+data['date']+'</span>'+
				'</div>'+
				'<div class="body_comment">'+data['text']+'</div>'+
			'</li>';
	comments.append($(html));
}

function sendComment(heart_id) {
	var comment = {};
	comment['heart_id'] = heart_id;
	comment['author'] = $("#author").val();
	comment['text'] = $("#message").val();
	$.ajax({
		type: "POST",
		url: "comments/new",
		dataType: "JSON",
		cache: false,
		data: {Comment: comment}
	}).done(function(res) {
		console.log(res);
	});
}