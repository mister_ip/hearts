<?php
/* @var $this SiteController */
$this->beginClip('сontent');
$this->pageTitle=Yii::app()->name;
?>

<div id="wrap">
	<div id="header">
		<!-- <div id="logo">
			<img src="hlogo.jpg" />
			<h1>HEARRRRRTS!</h1>
		</div>-->
		<div id="buttons">
			<a href="#" id='wantheart'>Хочу сердце!</a>
			<!--<a href="2">Я организатор конкурса!</a>-->
		</div>
		<div style="clear: both">&nbsp;</div>
		<div id="login">
			<?php if(Yii::app()->user->isGuest) echo '<a href="https://oauth.vk.com/authorize?client_id=3993476&scope=notify&redirect_uri=http://vmatveev.shpp.me/hearts/user/vk&response_type=code&v=5.3" title="Зайти через ВКонтакте"><img src="images/swv.png"/></a>'; ?>
			<?php if(Yii::app()->user->isGuest) echo '<a href="https://www.facebook.com/dialog/oauth?client_id=216688568503053&redirect_uri=http://vmatveev.shpp.me/hearts/user/fb&response_type=code"><img src="images/swf.png"/></a>'; ?>
			<?php if(Yii::app()->user->isGuest) echo '<a href="#"><img src="images/swt.png"/></a>'; ?>
			<?php if(Yii::app()->user->isGuest) echo '<a id="login-btn" class="login-bar"><i class="fa fa-sign-in"></i></a>'; ?>
			<?php if(!Yii::app()->user->isGuest) echo '<a href="user/logout" class="login-bar"><i class="fa fa-sign-out"></i></i></a>'; ?>
		</div>
		
	</div>
	<div id="body">
		<div id="center">

			<div id="form">
				<div id="login-form" >
					<button id="remove-login" class='remove-btn'><i class='fa fa-times'></i></button>
					<div class="form-lb">
						<label for="form_login">login</label>
						<input class="input" type="text" id="form_login"/>
					</div>
					<div class="form-lb">
						<label for="form_pass">password</label>
						<input class="input" type="password" id="form_pass"/>
					</div>
					<button class="btn" onclick="login()">login</button>
					<button class="btn" id="sign-up-btn" onclick="initSignUpForm()">sing up</button>
				</div>
				<div id="sign-up-form">
					<button id="remove-sign-up" class='remove-btn'><i class='fa fa-times'></i></button>
					<div class="form-lb">
						<label for="sign_up_email">email</label>
						<input class="input" type="email" name="User[email]" id="sign_up_email"/>
					</div>
					<div class="form-lb">
						<label for="sign_up_login">login</label>
						<input class="input" type="text" name="User[login]" id="sign_up_login"/>
					</div>
					<div class="form-lb">
						<label for="sign_up_name">name</label>
						<input class="input" type="text" name="User[name]" id="sign_up_name"/>
					</div>
					<div class="form-lb">
						<label for="sign_up_pass">password</label>
						<input class="input" type="password" name="User[password]" id="sign_up_pass"/>
					</div>
					<div class="form-lb">
						<label for="sign_up_rpass">again password</label>
						<input class="input" type="password" id="sign_up_rpass"/>
					</div>
					<button id="sign-up" class="btn" onclick="signUp()">sign up</button>
				</div>
			</div>

			<div id="left">
				<div id="info-contest" class="thelist">
					<p>Информация о конкурсе</p>
				</div>
				<div id="heart_editor" class="thelist">
					<button class="remove-btn" onclick="removeSelected()"><i class="fa fa-times"></i></button>
					<p>Редактор сердец</p>
					<div id="top-btn">
						<label for="heart_size">
							<select id="heart_size">
								<option value="20">micro</option>
								<option value="30">mini</option>
								<option value="40">small</option>
								<option value="50" selected>normal</option>
								<option value="60">big</option>
								<option value="100">large</option>
							</select>
						</label>
						<label for="heart_color">
							<select id="heart_color">
								<option value="#ff887c">Red</option>
								<option value="#7bd148">Green</option>
								<option value="#5484ed">Bold blue</option>
								<option value="#46d6db">Turquoise</option>
								<option value="#51b749">Bold green</option>
								<option value="#fbd75b">Yellow</option>
								<option value="#ffb878">Orange</option>
								<option value="#dc2127">Bold red</option>
								<option value="#dbadff">Purple</option>
								<option value="#e1e1e1">Gray</option>
							</select>
						</label>
					</div>
					<div id="screen_editor"></div>
					<div id="panel_editor">
						<div class="wrap_editor top_panel">
							<button class="btn" id="heart_turn_r"><i class="fa fa-repeat"></i></button>
							<button class="btn" id="heart_turn_l"><i class="fa fa-undo"></i></button>
							<button class="btn" id="locate" onclick="newHeart()">разместить</button>
						</div>
						<div class="user_info wrap_editor">
							<div class="form-lb">
								<label for="info_title">заголловок</label>
								<input class="input" id="info_title" type="text">
							</div>
							<div class="form-lb">
								<div class="wrap_editor">
									<label for="file">картинка</label>
									<div id="image"></div>
									<input class="input" id="uploadFile" placeholder="Choose File" disabled="disabled">
									<div class="fileUpload btn">
										<span>выбрать</span>
										<input id="file" type="file" class="upload">
									</div>
									<button class="btn" onclick="saveFile()" style="height: 30px; padding-top: 6px;">загрузить</button>
								</div>
							</div>
							<div class="form-lb">
								<label for="info_text">текст</label>
								<textarea id="info_text" rows="6"></textarea>
							</div>
							<button id="buy" class="btn" onclick="saveHeart()">купить</button>
						</div>
					</div>
				</div>
			</div>
			<div id="screen"></div>
			<div class="btn-ipad"></div>
			<div id="info"></div>
			<div id="right">
				<div id="tophearts" class="thelist">
					<p>TOP сердца</p>
					<ul>
						<?php foreach ($top_hearts as $heart) : ?>
							<li><a id="<?php echo $heart->id; ?>" class="tada"><?php echo $heart->title ?></a></li>
						<?php endforeach; ?>
					</ul>
				</div>
				<div id="activity" class="thelist">
					<p>Активные участники</p>
					<ul>
						<?php foreach ($top_users as $user) : ?>
							<li><a class="auser tada" id="u<?php echo $user->id ?>"><?php echo $user->name ?></a></li>
						<?php endforeach; ?>
					</ul>
				</div>
			</div>
		</div>
		<div id="comments">
			<ul id="comments_list"></ul>
		</div>
		<div id="form_comment" class="comment">
			<?php if (Yii::app()->user->isGuest): ?>
				<div class="form-lb">
					<label for="author">Ваше имя</label><br/>
					<input class="input" id="author" type="text"><br/>
				</div>
			<?php endif; ?>
			<div class="form-lb">
				<label for="message">Ваш комментарий</label>
				<textarea class="input" id="message"></textarea>
			</div>
			<div class="form-lb">
				<button id="send-mess" class="btn">send</button>
			</div>
		</div>
	</div>
</div>
<?php $this->endClip(); ?>