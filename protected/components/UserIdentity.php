<?php

class UserIdentity extends CUserIdentity
{
	private $_id;
	const ERROR_STATUS_NOTACTIV=3;

	public function authenticate() {
		$user = Users::model()->find('LOWER(login)=?', array(strtolower($this->username)));

		if (!$user){
			$this->errorCode = self::ERROR_USERNAME_INVALID;
		}else if ($user->password !== md5 ($this->password)) {
			$this->errorCode = self::ERROR_PASSWORD_INVALID;
		}else {
			$this->_id=$user->id;
			$this->username=$user->name;
			$this->errorCode = self::ERROR_NONE;
			Yii::app()->session['userId'] = $user->id;
		}
		return !$this->errorCode;
	}

	public function recoverauthenticate($user) {
		$this->username=$user->login;
	}

	public function getId(){
		return $this->_id;
	}
}