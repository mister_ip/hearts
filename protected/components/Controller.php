<?php

class Controller extends CController
{
	public $layout = "//layouts/main";
	public $package = [];

	protected $error;
	protected $result = array();
	protected $user_id = 1;

	public  function init() {
		if($this->isIOS()) {
			$this->package['basePath'] = 'application.themes.iOS';
			$this->package['baseUrl'] = 'themes/iOS/';
			$this->package['css']		= array('css/main.css',
												'css/anim.css',
												'css/font-awesome.min.css',
												'css/jquery.simplecolorpicker.css');

			$this->package['js']		= array('js/jquery.min.js',
												'js/jquery.mobile-1.3.2.min.js',
												'js/anim.circles.js',
												'js/script.js',
												'js/jquery-ui-1.10.3.custom.min.js',
												'js/jquery-collision.min.js',
												'js/jquery.simplecolorpicker.js');
			Yii::app()->theme = 'iOS';
		}
		else {
			$this->package['basePath']	= 'application.themes.classic';
			$this->package['baseUrl']	= 'themes/classic/';
			$this->package['css']		= array('css/main.css',
												'css/anim.css',
												'css/font-awesome.min.css',
												'css/jquery.simplecolorpicker.css');

			$this->package['js']		= array('js/jquery.min.js',
												'js/script.js',
												'js/jquery-ui-1.10.3.custom.min.js',
												'js/jquery-collision.min.js',
												'js/jquery.simplecolorpicker.js');
		}

		$this->user_id = Yii::app()->user->getId() || 0;

		Yii::app()->clientScript->addPackage('main', $this->package)->registerPackage('main');
	}

	protected function give_the_result() {
		$r = array();
		if ($this->error) {
			$r['e'] = 1;
			$r['d'] = $this->error;
		}
		else {
			$r['e'] = 0;
			$r['r'] = $this->result;
		}
		echo json_encode($r);
	}

	protected function update_rating($user) {
		$user->rating = $user->hearts + $user->likes + $user->visits;
		$user->save(false);
	}

	protected function isIOS()
	{
		$iPod    = stripos($_SERVER['HTTP_USER_AGENT'],"iPod");
		$iPhone  = stripos($_SERVER['HTTP_USER_AGENT'],"iPhone");
		$iPad    = stripos($_SERVER['HTTP_USER_AGENT'],"iPad");
		$Android = stripos($_SERVER['HTTP_USER_AGENT'],"Android");
		$webOS   = stripos($_SERVER['HTTP_USER_AGENT'],"webOS");

		if($iPod || $iPad || $iPhone) return true;

		return false;
	}
}