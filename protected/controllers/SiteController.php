<?php

class SiteController extends Controller
{
	public function actionIndex()
	{
		$hearts = Hearts::model()->findAll(array('order'=>'votes DESC', 'limit' => 5, "condition"=>"visible = 1"));
		$users = Users::model()->findAll(array('order' => 'rating DESC', 'limit' => 5));
		$this->render('index', array('top_hearts'=>$hearts, 'top_users'=>$users));
	}

	public function actionError()
	{
		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}
}