<?php

class CommentsController extends Controller
{
	public function actionGet()
	{
		if(isset($_REQUEST['id'])) {
			$heart_id = (int)$_REQUEST['id'];

			$criteria = new CDbCriteria();
			$criteria->condition='heart_id=:id AND visible';
			$criteria->params=array(':id'=>$heart_id);
			$criteria->order = "date_post DESC";
			$criteria->limit = "10";
			$comments = Comments::model()->findAll($criteria);
			if ($comments) {
				foreach ($comments as $comment) {
					array_push($this->result, array(
						'id'=>$comment->id,
						'date'=>$comment->date_post,
						'author'=>$comment->author,
						'text'=>$comment->text,
						'rating'=>$comment->rating
					));
				}
			}
			else {
				$this->error = "comments not found";
			}
		}
		else {
			$this->error = "POST['id'] is empty";
		}
		$this->give_the_result($this->result);
	}

	public function actionNew()
	{
		if (isset($_REQUEST['Comment'])) {
			if ($_REQUEST['Comment']['author']) {
				$author = $_REQUEST['Comment']['author'];
			}
			else {
				$author = Yii::app()->user->name;
			}
			$comment = new Comments;
			$comment ->attributes = $_REQUEST['Comment'];
			$comment->date_post = 	date('Y-m-d H:i:s');
			$comment->author = 		$author;
			$comment->rating = 		0;
			$comment->visible = 	1;
			if($comment->validate()) {
				$comment->save(false);
			}
			else {
				$this->error = json_decode(CActiveForm::validate($comment));
			}
		}
		else {
			$this->error = "POST['Comment'] is empty";
		}
		$this->give_the_result($this->result);
	}
}