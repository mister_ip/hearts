<?php

class HeartsController extends Controller
{
	public function actionGetAll()
	{
		$hearts = Hearts::model()->findAll(array("condition"=>"visible = 1"));
		foreach ($hearts as $heart) {
			$el['params'] = json_decode($heart->params, true);
			$el['id'] = 	$heart->id;
			$el['title'] = 	$heart->title;
			$el['img'] = 	$heart->img;
			$el['text'] = 	$heart->text;
			$el['votes'] = 	$heart->votes;
			array_push($this->result, $el);
		}
		$this->give_the_result($this->result);
	}

	public function actionSave(){
		if ($_REQUEST['heart']) {
			$user = Users::model()->findByPk($this->user_id);
			if ($user) {
				date_default_timezone_set('Europe/Kiev');
				$data = $_REQUEST['heart'];
				$heart = new Hearts;
				$heart->user_id =	$this->user_id;
				$heart->visible = 	0;
				$heart->title =		$data['title'];
				$heart->params =	json_encode($data['params']);
				$heart->img =		$data['img'];
				$heart->text =		$data['text'];
				$heart->votes =		0;
				$heart->created =	date('Y-m-d H:i:s');
				$heart->save();

				$user->hearts += 1;
				$this->update_rating($user);
				$this->result['id'] = $heart->id;
			}
			else {
				$this->error = "User not registered";
			}
		}
		else {
			$this->error = "POST['heart'] is empty";
		}
		$this->give_the_result($this->result);
	}

	public function actionUpdate(){
		if ($_REQUEST['heart']) {
			$data = $_REQUEST['heart'];
			$id = (int) $data['id'];
			$heart = Hearts::model()->findByPk($id);
			$heart->title =		$data['title'];
			$heart->params =	json_encode($data['params']);
			$heart->img =		$data['img'];
			$heart->text =		$data['text'];
			$heart->save();
		}
		else {
			$this->error = "POST['id'] or POST['heart'] is empty";
		}
		$this->give_the_result($this->result);
	}

	public function actionVote() {
		if ($_REQUEST['vote'] && $_REQUEST['id']) {
			$vote = (int) $_REQUEST['vote'];
			$id = (int)$_REQUEST['id'];
			$heart = Hearts::model()->findByPk($id);
			$user = Users::model()->findByPk($heart->user_id);
			if ($vote == 1) {
				$heart->votes += 1;
				$user->likes += 1;
			}
			if ($vote == -1) {
				$heart->votes -= 1;
				$user->likes -= 1;
			}
			$heart->save();
			$this->update_rating($user);

			$this->result['votes'] = $heart->votes;
		}
		else {
			$this->error = "POST['id'] or POST['heart'] is empty";
		}
		$this->give_the_result($this->result);
	}

	public function actionUser()
	{
		if ($_REQUEST['user_id']) {
			$id = (int)$_REQUEST['user_id'];
			$hearts = Hearts::model()->findAll(array("condition"=>"user_id =  $id"));
			foreach ($hearts as $heart) {
				$el['id'] = 	$heart->id;
				array_push($this->result, $el);
			}
		}
		else {
			$this->error = "user_id is empty";
		}
		$this->give_the_result($this->result);
	}

	public function actionFile()
	{
		$user_id = 1;

		$path = YiiBase::getPathOfAlias('webroot') . "/upload/" .  $user_id;
		$allowedExts = array("gif", "jpeg", "jpg", "png");
		$temp = explode(".", $_FILES["file"]["name"]);
		$extension = end($temp);
		if ((($_FILES["file"]["type"] == "image/gif")
				|| ($_FILES["file"]["type"] == "image/jpeg")
				|| ($_FILES["file"]["type"] == "image/jpg")
				|| ($_FILES["file"]["type"] == "image/pjpeg")
				|| ($_FILES["file"]["type"] == "image/x-png")
				|| ($_FILES["file"]["type"] == "image/png"))
			&& ($_FILES["file"]["size"] < 500000)
			&& in_array($extension, $allowedExts))
		{
			if ($_FILES["file"]["error"] > 0)
			{
				$this->error = "Return Code: " . $_FILES["file"]["error"];
			}
			else
			{
				if (is_dir($path) || mkdir($path, 0777)) {
					move_uploaded_file($_FILES["file"]["tmp_name"], $path. "/" . $_FILES["file"]["name"]);
					$this->result['src'] = Yii::app()->getBaseUrl(true) . '/upload/'.$user_id.'/'.$_FILES["file"]["name"];
				}
			}
		}
		else
		{
			$this->error = "Invalid file";
		}
		$this->give_the_result($this->result);
	}

	public function actionConformation()
	{
		echo "1";
		if (isset($_REQUEST['id'])) {
			$id_heart = $_REQUEST['id'];

			$heart = Hearts::model()->findByPk($id_heart);
			$heart->visible = 1;
			$heart->save();

			$this->redirect(array('/site'));
		}
	}
}