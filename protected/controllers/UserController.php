<?php

class UserController extends Controller
{

	public function actionSignup()
	{
		if(isset($_POST['User']))
		{
			$user = new Users;
			$user->attributes = $_POST['User'];
			if($user->validate()) {
				$user->save(false);
			}
			else {
				$this->error = json_decode(CActiveForm::validate($user));
			}
		}
		$this->give_the_result($this->result);
	}

	public function actionLogin()
	{
		if (!defined('CRYPT_BLOWFISH')||!CRYPT_BLOWFISH)
			throw new CHttpException(500,"This application requires that PHP was compiled with Blowfish support for crypt().");

		$model = new LoginForm;

		if(isset($_POST['LoginForm']))
		{
			$model->attributes=$_POST['LoginForm'];
			if($model->validate() && $model->login()) {
				$user = Users::model()->find('LOWER(login)=?', array(strtolower($model->login)));
				$user->visits += 1;
				$this->update_rating($user);
				Yii::app()->session['user'] = Yii::app()->user->name;
			}
			else {
				$this->error = json_decode(CActiveForm::validate($model));
			}
		}
		$this->give_the_result($this->result);
	}

	public function actionLogout()
	{
		Yii::app()->user->logout();
		Yii::app()->session->clear();
		$this->redirect(array('/'));
	}

	public function actionVk()
	{
		if (isset( $_REQUEST['code'])) {
			$url = 'https://oauth.vk.com/access_token';
			$qry_str = "?client_id=3993476".
						"&client_secret=BJGa8cwGrlQeKOiP4ovl".
						"&redirect_uri=http://vmatveev.shpp.me/hearts/user/vk".
						"&code=".$_REQUEST['code'];

			$data = json_decode($this->get_curl($url, $qry_str));
			$user = Users::model()->find('LOWER(login)=?', array(strtolower("vk".$data->user_id)));
			if($user) {
				$this->oauth_login($user);
			}
			else {
				$url = "https://api.vk.com/method/users.get";
				$qry_str = "?uids={$data->user_id}".
					"&fields=uid,first_name,last_name,nickname,screen_name,sex,bdate,city,country,timezone,photo".
					"&access_token={$data->access_token}";

				$data_user = json_decode($this->get_curl($url, $qry_str));
				$this->oauth_sign_up( "vk".$data->user_id, $data_user->response[0]->first_name." ".$data_user->response[0]->last_name, "vk");
			}
		}
		$this->redirect(array('/'));
	}

	public function actionFb()
	{
		if(isset($_REQUEST['code'])) {
			$url = 'https://graph.facebook.com/oauth/access_token';
			$qry_str = "?client_id=216688568503053".
				"&client_secret=d4ca5601140488ee93da1d431326e0df".
				"&redirect_uri=http://vmatveev.shpp.me/hearts/user/fb".
				"&code=".$_REQUEST['code'];
			$data = $this->get_curl($url, $qry_str);
			preg_match('/access_token=([^&]+)/', $data, $matches);
			if (isset($matches[1])) {
				$url = 'https://graph.facebook.com/me';
				$qry_str = "?access_token=".$matches[1];
				$data = json_decode($this->get_curl($url, $qry_str));
				$user = Users::model()->find('LOWER(login)=?', array(strtolower("fb".$data->id)));
				if($user) {
					$this->oauth_login($user);
				}
				else {
					$this->oauth_sign_up( "fb".$data->id, $data->name, "fb");
				}
			}
			$this->redirect(array('/'));
		}
	}

	protected function get_curl($url, $qry_str)
	{
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url.$qry_str);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_TIMEOUT, '3');
		$data = trim(curl_exec($ch));
		curl_close($ch);
		return $data;
	}

	protected function oauth_login($user)
	{
		$model = new LoginForm;
		$model->login = $user->login;
		$model->password = md5($user->login);
		$model->login();
		$user->visits += 1;
		$this->update_rating($user);
		Yii::app()->session['user'] = Yii::app()->user->name;
	}

	protected function oauth_sign_up($login, $name, $email)
	{
		$user = new Users;
		$user->login = $login;
		$user->name = 	$name;
		$user->email =	 $email;
		$user->password = md5($login);
		$user->save(false);

		$this->oauth_login($user);
	}
}