<?php

class Hearts extends CActiveRecord
{
	public function tableName()
	{
		return 'hearts';
	}

	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('created, user_id, params, img, votes, text', 'required'),
			array('votes', 'numerical', 'integerOnly'=>true),
			array('user_id', 'length', 'max'=>10),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, created, user_id, params, img, votes, text', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'created' => 'Created',
			'user_id' => 'User',
			'params' => 'Params',
			'img' => 'Img',
			'votes' => 'Votes',
			'text' => 'Text',
		);
	}

	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('created',$this->created,true);
		$criteria->compare('user_id',$this->user_id,true);
		$criteria->compare('params',$this->params,true);
		$criteria->compare('img',$this->img,true);
		$criteria->compare('votes',$this->votes);
		$criteria->compare('text',$this->text,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Hearts the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}