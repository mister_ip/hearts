<?php

class Users extends CActiveRecord
{
	public function tableName()
	{
		return 'users';
	}

	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('login, password, email, name', 'required'),
			array('hearts, likes, visits', 'numerical', 'integerOnly'=>true),
			array('rating', 'numerical'),
			array('login', 'length', 'max'=>50),
			array('password, salt, email, name', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, login, password, salt, email, name, hearts, likes, visits, rating', 'safe', 'on'=>'search'),
		);
	}


	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'login' => 'Login',
			'password' => 'Password',
			'salt' => 'Salt',
			'email' => 'Email',
			'name' => 'Name',
			'hearts' => 'Hearts',
			'likes' => 'Likes',
			'visits' => 'Visits',
			'rating' => 'Rating',
		);
	}

	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('login',$this->login,true);
		$criteria->compare('password',$this->password,true);
		$criteria->compare('salt',$this->salt,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('hearts',$this->hearts);
		$criteria->compare('likes',$this->likes);
		$criteria->compare('visits',$this->visits);
		$criteria->compare('rating',$this->rating);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	protected function beforeSave()
	{
		if(parent::beforeSave())
		{
			if($this->isNewRecord)
			{
				$this->registered = date('Y-m-d');
				$this->password = $this->hashPassword($this->password);
				$this->hearts = 0;
				$this->likes = 0;
				$this->visits = 0;
				$this->rating = 0;
			}
			return true;
		}
		return false;
	}
	public function hashPassword($password)
	{
		return md5($password);
	}
}