<?php

/**
 * This is the model class for table "comments".
 *
 * The followings are the available columns in table 'comments':
 * @property string $id
 * @property string $date_post
 * @property integer heart_id
 * @property string $author
 * @property string $text
 * @property integer $rating
 * @property integer $visible
 */
class Comments extends CActiveRecord
{
	public function tableName()
	{
		return 'comments';
	}

	public function rules()
	{
		return array(
			array('date_post, heart_id, author, text, rating, visible', 'required'),
			array('rating, visible', 'numerical', 'integerOnly'=>true),
			array('author', 'length', 'max'=>50),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, date_post, author, text, rating, visible', 'safe', 'on'=>'search'),
		);
	}

	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'date_post' => 'Date Post',
			'heart_id' => 'Heart Id',
			'author' => 'Author',
			'text' => 'Text',
			'rating' => 'Rating',
			'visible' => 'Visible',
		);
	}

	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('date_post',$this->date_post,true);
		$criteria->compare('author',$this->author,true);
		$criteria->compare('text',$this->text,true);
		$criteria->compare('rating',$this->rating);
		$criteria->compare('visible',$this->visible);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

}